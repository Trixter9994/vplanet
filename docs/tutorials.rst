VPLanet Tutorials
=================

Please view the following pages for written descriptions of how to:

.. toctree::
   :maxdepth: 1

   Code Architecture <architecture>
   Add an Output <output>
   Add an Option <option>
   Add a Primary Variable <primaryvariable>
   Add a Module <module>
   Coupling Modules <coupling>
