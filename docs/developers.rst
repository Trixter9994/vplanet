Contributor's Guide
===================

Interested in joining the **VPLanet** community? Here we outline the steps to
make a meaningful contribution to the code, and the discovery of life beyond
the Solar System! Before you begin modifying the code, we recommend you
contact `Rory Barnes <mailto:rory@astro.washington.edu>` to confirm that your
proposed changes are not duplicating work and will be of general interest.

Since VPLanet is a large and abstract code, its sustainability depends on the
code's maintenance as it grows. We have therefore initiated a series of steps
that are required prior to any changes becoming part of the main branch. The
following sections provide details on each step and include some motivation for
this methodology.

.. toctree::
   :maxdepth: 1

   Style Guide <StyleGuide>
   Writing Tests, and Examples <tests>
   Attribution <attr>
